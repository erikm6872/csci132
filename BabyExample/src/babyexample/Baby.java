package babyexample;

/**
 * @author Erik McLaughlin
 * @date Sep 7, 2016
 */
public class Baby {
    private int weight;
    private String name, hairColor;

    public Baby(String name, int weight, String hairColor){
        this.name = name;
        this.weight = weight;
        this.hairColor = hairColor;
    }
    
    // Weight methods
    public int getWeight(){
        return weight;
    }
    public void eat(int ozEaten){
        weight += ozEaten;
    }
    public void defecate(int ozDefecated){
        weight -= ozDefecated;
    }
    
    // Name methods
    public String getName(){
        return name;
    }
    public void changeName(String name){
        this.name = name;
    }
    
    // Hair color methods
    public String getHairColor(){
        return hairColor;
    }
    public void changeHairColor(String hairColor){
        this.hairColor = hairColor;
    }
    
    // Activity methods
    public void play(){
        // Do nothing
    }
    public void sleep(){
        // Do nothing
    }
}
