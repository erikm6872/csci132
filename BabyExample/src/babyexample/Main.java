package babyexample;

/**
 * @author Erik McLaughlin
 * @date Sep 7, 2016
 */
public class Main {
    //Constants
    
    public static void main(String[] args) {
        
        Baby mike = new Baby("Mike", 20, "Brown");
        Baby sarah = new Baby("Sarah", 17, "Blonde");
        
        mike.eat(3);
        sarah.eat(3);
        
        mike.play();
        sarah.sleep();
        
        
    }

}
