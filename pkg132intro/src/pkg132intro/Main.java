package pkg132intro;


/**
 * @author Erik McLaughlin
 * @date Sep 7, 2016
 */
public class Main {

    private String name;
    public int age;
    protected boolean truth;

    Main() {
        name = "homer";
        age = 42;
    }

    Main(String name) {
        this.name = name;
        age = 12;
    }

    Main(String x, int in_a) {
        name = x;
        age = in_a;
    }

    public static void doBitMath(int a) {
        System.out.println(a > 8 ? 1 : 3);

        int num = 26;
        System.out.printf("%x\n", num);

        System.out.println((int) (num & 0xF0) >> 4);
    }

    public boolean testClarity(int input) {
        return false; //GC.WEST == input;
    }

    private void changeName(String name) {
        this.name = name;
    }

    private String getName() {
        return name;
    }

    public static void main(String[] args) {
        
        Main myMain = new Main();
        Main myStringMain;

        myStringMain = new Main("Lisa");

        Main second = myStringMain;

        second.changeName("Bart");

        System.out.println(myStringMain.getName());

        doBitMath(26);
    }
}
